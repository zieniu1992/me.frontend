import { ProductService } from './../../../../core/services/product/product.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CreateProduct } from 'src/app/shared/models/product/create-product.model';
import { Product } from 'src/app/shared/models/product/product.model';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
  @Output() product = new EventEmitter<Product>();

  name: string = '';
  price: number = 0;
  code: string = '';

  constructor(private productService:ProductService) { }

  ngOnInit(): void {
  }
 
  createProduct() {
    var newProduct = new CreateProduct(this.name, this.price, this.code);

    this.productService.createProduct(newProduct).subscribe((id) => {     
      this.product.emit(new Product(id,this.name,this.price,this.code));
      this.clearForm();
    });
  }

  /** Walidacja formularza */
  isValidNewProduct(): boolean {
    if (this.name != '' && this.price != 0 && this.code != '') {
      return true;
    }

    return false;
  }

 private clearForm() {
    this.name = '';
    this.price = 0;
    this.code = '';
  }
}
