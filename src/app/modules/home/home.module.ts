import { CreateProductComponent } from './components/create-product/create-product.component';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent,CreateProductComponent],
  imports: [CommonModule,FormsModule, MatTableModule,MatButtonModule,MatInputModule, MatFormFieldModule],
})
export class HomeModule {}
