import { map } from 'rxjs';
import { CreateProduct } from 'src/app/shared/models/product/create-product.model';
import { ProductService } from './../../core/services/product/product.service';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Product } from 'src/app/shared/models/product/product.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  displayedColumns: string[] = ['no', 'name', 'code', 'price'];
  products: Product[] = [];
  dataSource = new MatTableDataSource<Product>();


  constructor(
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.productService.fetchProducts().subscribe((src) => {
      this.products = src;
      this.dataSource.data = this.products;
    });
  }

  productCreated(product:Product) {
    this.products.push(product);
    this.dataSource.data = this.products;
  }
}
