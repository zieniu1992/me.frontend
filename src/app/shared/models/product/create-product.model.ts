export class CreateProduct {
  name: string;
  price: number;
  code: string;

  constructor(name: string, price: number, code: string) {
    this.name = name;
    this.price = price;
    this.code = code;
  }
}
