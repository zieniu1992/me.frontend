export class Product {
  id: string;
  name: string;
  price: number;
  code: string;

  constructor(id: string, name: string, price: number, code: string) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.code = code;
  }
}
