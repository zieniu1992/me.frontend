import { CreateProduct } from 'src/app/shared/models/product/create-product.model';
import { ProductRepository } from './../../repositories/product/product.repository';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private productRepository:ProductRepository) { 
    
  }

  fetchProducts() {
    return this.productRepository.fetchProducts();
  }

  createProduct(product:CreateProduct) {
    return this.productRepository.createProduct(product);
  } 
}
