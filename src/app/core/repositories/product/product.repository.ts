import { Product } from './../../../shared/models/product/product.model';
import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { CreateProduct } from 'src/app/shared/models/product/create-product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductRepository {

  constructor(private httpClient:HttpClient) { }

  
  fetchProducts() {
    return this.httpClient.get<Product[]>("/v1/Product/getAll");
  }

  createProduct(product:CreateProduct) {
    return this.httpClient.post<any>("/v1/Product/addProduct",product);
  }
}
